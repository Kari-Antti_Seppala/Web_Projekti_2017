

// funktio joka hakee elokuvan tiedot OMDB tietokannasta
// tiedot palautuvat JSON muodossa

function apiCall(haettavaElokuva, elokuvanNumero) {
    
    $.getJSON('https://omdbapi.com/?t=' + encodeURI(haettavaElokuva), function(data) {
       
       
      document.getElementById("title" + elokuvanNumero).innerHTML = data.Title;
      document.getElementById("released" + elokuvanNumero).innerHTML = data.Year;
      document.getElementById("plot" + elokuvanNumero).innerHTML = data.Plot;
      
      console.log(data);
      
    });
    
}

    apiCall('Star+Wars', 1);
    apiCall('Harry+Potter', 2);
    apiCall('Paul+Blart', 3);
      
